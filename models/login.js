const mongoose = require('mongoose')

const login = new mongoose.Schema({	
    userName: {type: String, trim: true, required: true},
    password: {type: String, trim: true, required: true},
})

module.exports = mongoose.model('Login', login)