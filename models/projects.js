const mongoose = require('mongoose')

const projects = new mongoose.Schema({	
    projName: {type: String, required: true},
    projID: {type: Number, required: true},
    projDuration: {type: Number},
    addedUsers: {type: array[String]},
    taskList: {type: array[String]},
    manager: {type: String, required: true},
    startDate: {type: Date},
    endDate: {type: Date},
    status: {type: String}
})

module.exports = mongoose.model('Projects', projects)