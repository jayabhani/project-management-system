const mongoose = require('mongoose')

const tasks = new mongoose.Schema({	
    taskID: {type: Number, required: true},
    domain: {type: String, required: true},
    projName: {type: String, required: true},
    users: {type: String},
    status: {type: String}
})

module.exports = mongoose.model('tasks', tasks)