const mongoose = require('mongoose')

const signUp = new mongoose.Schema({
	firstName: {type: String, trim: true, required: true}, 
	lastName: {type: String, trim: true, default:''},
	age: {type: Number, default: 0},
    userName: {type: String, trim: true, required: true},
    password: {type: String, trim: true, required: true},
    email: {type: String, trim: true, required: true},
    contactNo: {type: Number},
    profilePic: {type: String},
    Role: {type: String, default: 'developer'},
})

module.exports = mongoose.model('SignUp', signUp)